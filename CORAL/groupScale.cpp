#include "stdafx.h"
#include <iostream>
#include <fstream>
#include "Groups.h"
#include <ctime>

using namespace std;

void groupScale(const char * Filename, const char * outFilename)
{

	ifstream testDatasets;
    ofstream outputFile;        //saving the results in csv file

    testDatasets.open(Filename);    //txt file containing the names of datasets//
    outputFile.open(outFilename);
    outputFile<<"DataSet";
    outputFile<<',';
    outputFile<<"Group";
    outputFile<<',';
    outputFile<<"Time";
    outputFile<<endl;
    string filePaths="C:/Data/Datasets/";
    string outPath="C:/Data/Datasets/";
    char filename[100];
    int timeseriesN;
    int timeseriesL;
    string dataset;
    int numGroups;
   // outputFile.open(outFilename);
  //  int method=1;
    double ST=0.7;
    int numFiles=1;
    testDatasets>>numFiles;
    while(numFiles>0) //read all datasets one by one
    {
        testDatasets>>filename;
        dataset=filePaths+string(filename)+".txt";
        testDatasets>>timeseriesN;      //reading number of time series
        testDatasets>>timeseriesL;      //reading length of time series
        GroupOperation *groupObj=new GroupOperation(ST,timeseriesN,timeseriesL);     //ST passed as input
        groupObj->readFile(dataset.c_str(), timeseriesN,timeseriesL);

        int choice=1;   
        clock_t start = clock();
        numGroups= groupObj->groupOp(choice,100.0);
        clock_t end=(clock()-start)/double(CLOCKS_PER_SEC);
        outputFile<<filename;
        outputFile<<',';
        outputFile<<numGroups;
        outputFile<<',';
        outputFile<<end;    //printing the time
        outputFile<<endl;
        cout<<filename<<" "<<numGroups<<" "<<end<<"s"<<endl;
        delete groupObj;


        numFiles--;
    }
    outputFile.close();
}
