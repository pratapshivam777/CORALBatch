/***********************************************************************/
/************************* DISCLAIMER **********************************/
/***********************************************************************/
/**
/**                                                                   **/
/** Unless stated otherwise, all software is provided free of charge. **/
/** As well, all software is provided on an "as is" basis without     **/
/** warranty of any kind, express or implied. Under no circumstances  **/
/** and under no legal theory, whether in tort, contract,or otherwise,**/
/** shall be liable to you or to any other **/
/** person for any indirect, special, incidental, or consequential    **/
/** damages of any character including, without limitation, damages   **/
/** for loss of goodwill, work stoppage, computer failure or          **/
/** malfunction, or for any and all other damages or losses.          **/
/**                                                                   **/
/** If you do not agree with these terms, then you you are advised to **/
/** not use this software.                                            **/
/***********************************************************************/
/***********************************************************************/
//VERSION OF SYSTEM TO IMPLEMENT GDTW DISTANCES


#include <stdio.h>
#include <stdlib.h>
#include "Groups.h"
#include <string>
#include <time.h>
#include <iostream>
//#include "online.h"
//#include <sys/time.h>
#include <math.h>
#include <time.h>
#include "stdafx.h"
#include "BatchProcess.h"

using namespace std;

void groupScale(const char * Filename, const char * outFilename)
{

	ifstream testDatasets;
	ofstream outputFile;        //saving the results in csv file

	testDatasets.open(Filename);    //txt file containing the names of datasets//
	outputFile.open(outFilename);
	outputFile << "DataSet";
	outputFile << ',';
	outputFile << "Group";
	outputFile << ',';
	outputFile << "Time";
	outputFile << endl;
	string filePaths = "C:/Data/Datasets/";
	string outPath = "C:/Data/Datasets/";
	char filename[100];
	int timeseriesN;
	int timeseriesL;
	string dataset;
	int numGroups;
	// outputFile.open(outFilename);
	//  int method=1;
	double ST = 0.7;
	int numFiles = 1;
	testDatasets >> numFiles;
	while (numFiles>0) //read all datasets one by one
	{
		testDatasets >> filename;
		dataset = filePaths + string(filename) + ".txt";
		testDatasets >> timeseriesN;      //reading number of time series
		testDatasets >> timeseriesL;      //reading length of time series
		GroupOperation *groupObj = new GroupOperation(ST, timeseriesN, timeseriesL);     //ST passed as input
		groupObj->readFile(dataset.c_str(), timeseriesN, timeseriesL);

		int choice = 1;
		clock_t start = clock();
		numGroups = groupObj->groupOp(choice, 100.0);
		clock_t end = (clock() - start) / double(CLOCKS_PER_SEC);
		outputFile << filename;
		outputFile << ',';
		outputFile << numGroups;
		outputFile << ',';
		outputFile << end;    //printing the time
		outputFile << endl;
		cout << filename << " " << numGroups << " " << end << "s" << endl;
		delete groupObj;


		numFiles--;
	}
	outputFile.close();
}
/*
void preprocess(const char * Filename, int N, int L, double ST, string filePath)
{


        GroupOperation *groupObj=new GroupOperation(ST);     //ST passed as input
        groupObj->readFile(Filename, N,L);
        //groupObj->normalize();


        groupObj->timeFilePath=filePath;
        groupObj->timeFilePath.append("Time.txt");
        groupObj->GroupFilePath=filePath.append("Groups.txt");
        groupObj->groupResults.open("C:/Qt/Tools/QtCreator/bin/build-CORAL-Desktop_Qt_5_1_1_MinGW_32bit-Debug/debug/groupResults.csv");
        cout<<"Enter 0 for single group, 1 for Multiple groups, 2 for Onex grouping, 3 for Random Centroids, 4 for for MaxSeq"<<endl;
        int choice;
        cin>>choice;
        double delta=10.0;
        if(choice!=2)
        {
            cout<<"Enter delta"<<endl;
            cin>>delta;
        }
      //  groupObj->groupResults.close();
        clock_t start = clock();
        groupObj->groupOp(choice,delta);
        cout << "Offline Construction " << (clock()-start)/double(CLOCKS_PER_SEC) << "s]" << endl;
        //print groups and length index in file
        groupObj->printGroups();
        groupObj->printTime();

}
*/
using namespace std;
/* Following are the argc and argv definations for Option 10 and 11
arg1: MODE ( option number that choses program parts)
arg2: N (no. of rows in dataset)
arg3: L (no. of columns in dataset)
arg4: ST (parameter for cluster generation - look CORAL paper for more info)
arg5: Input dataset Path
arg6: Output file path (Output file contains output of program execution) - needed for option 11 
arg7: no. of runs (needed for option 11)
arg8: command file path (Command file contains extra input information) - needed for option 10, 11
*/


void main(int argc, char* argv[])
{
//	groupScale("C:/Data/Datasets/dataset.txt", "C:/Data/Datasets/outData.csv");
	//return 1;
    int arg1,arg2,arg3, arg5,arg6,arg7, arg8;
	
    double arg4;
    char *filename;
 //   string filenameMatrix;
    string queryFilename;
    //cout<<"Load data file, Enter filename followed by N L"<<endl;
    //cin>>filename;
    //cin>>arg2;//N
	//cin>>arg3;//L
	// arg2 = timeSeriesN;
	// arg3 = timeSeriesL;
    int queryLength;
    string filePath;

	arg1 = atoi(argv[1]);				// MODE
	arg2 = atoi(argv[2]);				// N
	arg3 = atoi(argv[3]);				// L
	arg4 = atof(argv[4]);				// ST
	filename = argv[5];					// Input dataset Path
	char * OutputFileAddr = argv[6]; 
	int nRuns = atoi(argv[7]);			// no. of runs
	char * CommandFileAddr = argv[8];


    //read data
    GroupOperation *groupObj=new GroupOperation(0.2,arg2,arg3);// ST, number of time series in original file, length of time series
    groupObj->readFile(filename,arg2,arg3);
    cout<<"Time series read"<<endl;

  //  onlineObj->gdtw();

	// cout << "Enter 0 to preprocess the data, 1 to find correlation through brute force, 2 through Coral, 3 to find correlation bw 2 sequences, 4 to find naiveselfcorr, 5 to find CORAL self correlation, 6 to find CORAL longest, 7 to find NaiveLongest, 8 to get NAIVE groups of correlated sequences, 9 to get CORAL groups, 10 to batch process, 11 to exit" << endl;
    // cin>>arg1;
	//arg1 = 0;

    while(arg1!=12)
    {
        if(arg1==0)
        {
            cout<<"Enter ST"<<endl;
            cin>>arg4;//ST
			//arg4 = 0.35; // ST
			groupObj->ST = arg4;	//set ST
            //cout<<"Enter path for writing groups and time files"<<endl;
            //cin>>filePath;

            //groupObj->timeFilePath=filePath;
           // groupObj->timeFilePath.append("Time.txt");
          //  groupObj->GroupFilePath=filePath.append("Groups.txt");
       //     groupObj->groupResults.open("C:/Qt/Tools/QtCreator/bin/build-CORAL-Desktop_Qt_5_1_1_MinGW_32bit-Debug/debug/groupResults.csv");
            cout<<"Enter 0 for optimized , 1 for Multiple groups, 2 for Greedy grouping"<<endl;
            int choice;
            cin>>choice;
			//choice = 0;
            double delta=10.0;
            if(choice!=2)
            {
                cout<<"Enter delta"<<endl;
                cin>>delta;
				//delta = 100;
            }
          //  groupObj->groupResults.close();
            clock_t start = clock();
            int TotalGroups = groupObj->groupOp(choice,delta);
            cout << "Offline Construction " << (clock()-start)/double(CLOCKS_PER_SEC) << "s]" << endl;
			cout << "Total Groups " << TotalGroups << endl;
			groupObj->IsGroupArrayInMem = true;
			//	UNCOMMENT WHEN WANT TO WRITE THE FILE
			/*
			cout << "Enter path for writing groups and time files" << endl;
			cin >> filePath;
			groupObj->timeFilePath=filePath;
			 groupObj->timeFilePath.append("Time.txt");
			  groupObj->GroupFilePath=filePath.append("Groups.txt");
            //print groups and length index in file
            groupObj->printGroups();
            groupObj->printTime();
			*/
			



/*
            cout<<"Enter path for reading groups and time file"<<endl;
            cin>>filePath;
            onlineObj->timeFilePath=filePath;
           onlineObj->timeFilePath.append("Time.txt");
            onlineObj->readTime(onlineObj->timeFilePath.c_str());
            onlineObj->GroupFilePath=filePath.append("Groups.txt");
            //read groups genereted in offline step
            onlineObj->readGroups(onlineObj->GroupFilePath.c_str());
            */

        }
        else if(arg1==1)
        {
            cout<<" BRUTE FORCE"<<endl;
			clock_t start1;
            //cout<<"preprocess filename"<<endl;
			groupObj->tempQ.clear();	//clear the query vector
            cout<<"Enter 1 to load query from time series, 2 from query file"<<endl;
            cin>>arg2;
            if(arg2==1) //load from time series
            {
                cout<<"<queryTS, start, end"<<endl;
                cin>>arg2;
                cin>>arg3;
                cin>>arg5;
				//start1 = clock();
               groupObj->loadQueryTS(arg2,arg3,arg5);
            }
            else
            {
                cout<<"<queryFile, query length"<<endl;
                cin>>queryFilename;
                cin>>queryLength;
				
                groupObj->readQueryFile(queryLength,queryFilename.c_str(),false);
            }
			start1 = clock();

        
            
          // cout<<"Spring Calculation"<<endl;
            groupObj->springCalculation();//onlineObj->tempQ.size()-1);
           cout << "Naive Time" << (clock()-start1)/double(CLOCKS_PER_SEC) << "s]" << endl;


        }
        else if(arg1==2)
        {
			//CORAL
			groupObj->tempQ.clear();	//clear the query vector
            cout<<"Enter 1 to load query from time series, 2 from query file"<<endl;
            cin>>arg2;
            if(arg2==1) //load from time series
            {
                cout<<"<queryTS, start, end"<<endl;
                cin>>arg2;
                cin>>arg3;
                cin>>arg5;
               groupObj->loadQueryTS(arg2,arg3,arg5);
            }
            else
            {
                cout<<"<queryFile, query length"<<endl;
                cin>>queryFilename;
                cin>>queryLength;
                groupObj->readQueryFile(queryLength,queryFilename.c_str(),false);
            }


            int queryLength=groupObj->tempQ.size();
            //z-normalize query sequence
            groupObj->znormSequenceS(groupObj->tempQ);
            clock_t start1 = clock();
            //search only same length
            groupObj->kSimilarnew(queryLength);

          //  cout<<"Best distance "<<dist<<endl;
            cout << (clock() - start1) / double(CLOCKS_PER_SEC) << "s]"<<"Coral " << endl;
			cout << "Seq Explored " << groupObj->seqExplored << " Total Seqs " << (groupObj->N * (groupObj->L - groupObj->tempQ.size() + 1)) << endl;

            //cout<<"Best TS "<<onlineObj->kbest.id<<" ["<<onlineObj->kbest.startT<<" "<<onlineObj->kbest.endT<<"]"<<endl;

        }
        /*
        else if(arg1==3)    //group for multiple datasets
        {
            cout<<"Enter file name with full path"<<endl;
            string inputFilePath;
            string outputFilePath;
            cin>>inputFilePath;
            cout<<"Enter output file path"<<endl;
            cin>>outputFilePath;
            groupScale(inputFilePath.c_str(),outputFilePath.c_str());


        }
        */
        else if(arg1==3)    //find correlation between 2 sequences
        {
            cout<<"seq1 start end seq2 start end method"<<endl;
            cin>>arg2;//seq1
            cin>>arg3;//start
            cin>>arg4;//end
            cin>>arg5;//seq2
            cin>>arg6;//start
            cin>>arg7;//end
            groupObj->corrSequences(arg2,arg3,arg4,arg5,arg6,arg7);
        }
		else if(arg1 == 4)	//Naive self correlation
		{
			cout << "Enter time series id, length and correlation threshold"<<endl;
			cin >> arg2;
			cin >> arg3;
			cin >> arg4;
			clock_t start1 = clock();
			groupObj->selfCorrNaive(arg2, arg3, arg4);
			cout <<endl<< "Naive Self Corr Time" << (clock() - start1) / double(CLOCKS_PER_SEC) << "s]" << endl;

		}
		else if (arg1 == 5)		//CORAL self correlation 
		{
			cout << "Enter time series id and length"<<endl;
			cin >> arg2;
			cin >> arg3;
			
			clock_t start1 = clock();
			groupObj->selfCorrelation(arg2, arg3);
			cout << "Corr Time" << (clock() - start1) / double(CLOCKS_PER_SEC) << "s]" << endl;
		}
		else if (arg1 == 6)	//CORAL longest length sequences
		{
			cout << "Enter time series 1, time series 2, min Length" << endl;
			cin >> arg2;
			cin >> arg3;
			cin >> arg5;
			clock_t start1 = clock();
			groupObj->longestCORAL(arg2, arg3, arg5);
			cout << "Corr Time" << (clock() - start1) / double(CLOCKS_PER_SEC) << "s]" << endl;

		}
		else if (arg1 == 7)	//Naive Longest sequences
		{
			cout << "Enter time series 1, time series 2, min Length and correlation threshold" << endl;
			cin >> arg2;
			cin >> arg3;
			cin >> arg5;
			cin >> arg4;
			clock_t start1 = clock();
			groupObj->longestCorrNaive(arg2, arg3, arg5, arg4);
			cout << "Naive Time" << (clock() - start1) / double(CLOCKS_PER_SEC) << "s]" << endl;


		}
		else if (arg1 == 8)	//Naive groups
		{
			cout << "Enter length and correlation threshold" << endl;;
			cin >> arg5;
			cin >> arg4;
			clock_t start1 = clock();
			groupObj->naiveGroupCorr(arg4, arg5, false);		//set to false when testing for different thresholds 
			cout << "Naive Groups Time" << (clock() - start1) / double(CLOCKS_PER_SEC) << "s]" << endl;

		}
		else if (arg1 == 9)	//CORAL groups
		{
			cout << "Enter length and correlation threshold"<<endl;
			cin >> arg5;
			cin >> arg4;
			clock_t start1 = clock();
			groupObj->getGroupsSeq(arg5, arg4, false);		//set to false when testing for different thresholds 
			cout << "Corr Time" << (clock() - start1) / double(CLOCKS_PER_SEC) << "s]" << endl;

		}

		else if (arg1 == 10)
		{
			BatchProcess h_BatchProcess(groupObj);
			h_BatchProcess.SetInputCommFileAddr(CommandFileAddr);
			h_BatchProcess.SetTimeSeriesN(arg2);
			h_BatchProcess.SetTimeSeriesL(arg3);
			h_BatchProcess.Run();
			arg1 = 12;
		}

		else if (arg1 == 11)	//run other batch operations like groups of correlated sequence, self correlation and longest length correlation
		{
			BatchProcess h_BatchProcess(groupObj);
			h_BatchProcess.SetTimeSeriesN(arg2);	//set N
			h_BatchProcess.SetTimeSeriesL(arg3);	//set L
			h_BatchProcess.SetST(arg4);				//set ST
			h_BatchProcess.SetInputDataSet(filename);	//set input filename 
			h_BatchProcess.SetNumberRuns(nRuns);	//number of query runs
			h_BatchProcess.SetOutputFile(OutputFileAddr);
			h_BatchProcess.SetChoice(0);
			h_BatchProcess.SetDelta(100);
			h_BatchProcess.runOtherOperations();			//run other operations
			arg1 = 12;
		}

        cout<<"Enter 0 to preprocess the data, 1 to find correlation through brute force, 2 through Coral,3 to find correlation bw 2 sequences, 4 to find naiveselfcorr,5 to find CORAL self correlation, 6 to find CORAL longest, 7 to find NaiveLongest, 8 to get NAIVE groups of correlated sequences, 9 to get CORAL groups, 10 to batch process, 11 to exit"<<endl;
        // cin>>arg1;
    }


}


